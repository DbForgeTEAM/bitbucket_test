﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[udfMinimumDate] (
    @xx DATETIME, 
    @y DATETIME
) RETURNS DATETIME
AS
BEGIN
    DECLARE @z DATETIME

    IF @xx <= @y 
        SET @z = @xx 
    ELSE 
        SET @z = @y

    RETURN(@z)
END;
GO