﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
/* vAssocSeqOrders supports assocation and sequence clustering data mmining models.
      - Limits data to FY2004.
      - Creates order case table and line item nested table.*/
CREATE VIEW [dbo].[vAssocSeqOrders]
AS
SELECT DISTINCT OrderNumber, CustomerKey, Region, IncomeGroup, OrderNumber
FROM         dbo.vDMPrep
WHERE     (FiscalYear = '2013')

GO