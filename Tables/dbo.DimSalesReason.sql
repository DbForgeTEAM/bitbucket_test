﻿CREATE TABLE [dbo].[DimSalesReason] (
  [SalesReasonKey] [int] IDENTITY,
  [SalesReasonAlternateKey] [int] NOT NULL,
  [SalesReasonName] [nvarchar](50) NOT NULL,
  [SalesReasonReasonType] [nvarchar](50) NOT NULL,
  CONSTRAINT [PK_DimSalesReason_SalesReasonKey] PRIMARY KEY CLUSTERED ([SalesReasonKey])
)
ON [PRIMARY]
GO