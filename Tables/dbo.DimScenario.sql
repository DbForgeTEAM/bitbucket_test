﻿CREATE TABLE [dbo].[DimScenario] (
  [ScenarioKey] [int] IDENTITY,
  [ScenarioName] [nvarchar](50) NULL,
  CONSTRAINT [PK_DimScenario] PRIMARY KEY CLUSTERED ([ScenarioKey])
)
ON [PRIMARY]
GO