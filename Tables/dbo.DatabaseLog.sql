﻿CREATE TABLE [dbo].[DatabaseLog] (
  [DatabaseLogID] [int] IDENTITY,
  [PostTime] [datetime] NOT NULL,
  [PostTime2] [datetime] NOT NULL,
  [DatabaseUser] [sysname] NOT NULL,
  [Event] [sysname] NOT NULL,
  [Schema] [sysname] NULL,
  [Object] [sysname] NULL,
  [TSQL] [nvarchar](max) NOT NULL,
  [Object1] [sysname] NULL,
  [XmlEvent] [xml] NOT NULL,
  CONSTRAINT [PK_DatabaseLog_DatabaseLogID] PRIMARY KEY NONCLUSTERED ([DatabaseLogID])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO