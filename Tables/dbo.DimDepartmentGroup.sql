﻿CREATE TABLE [dbo].[DimDepartmentGroup] (
  [DepartmentGroupKey] [int] IDENTITY,
  [ParentDepartmentGroupKey] [int] NULL,
  [DepartmentGroupName] [nvarchar](50) NULL,
  CONSTRAINT [PK_DimDepartmentGroup] PRIMARY KEY CLUSTERED ([DepartmentGroupKey])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[DimDepartmentGroup]
  ADD CONSTRAINT [FK_DimDepartmentGroup_DimDepartmentGroup] FOREIGN KEY ([ParentDepartmentGroupKey]) REFERENCES [dbo].[DimDepartmentGroup] ([DepartmentGroupKey])
GO