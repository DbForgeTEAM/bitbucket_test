﻿CREATE TABLE [dbo].[FactCallCenter] (
  [FactCallCenterID] [int] IDENTITY,
  [DateKey] [int] NOT NULL,
  [WageType] [nvarchar](15) NOT NULL,
  [Shift] [nvarchar](20) NOT NULL,
  [LevelOneOperators] [smallint] NOT NULL,
  [LevelTwoOperators] [smallint] NOT NULL,
  [TotalOperators] [smallint] NOT NULL,
  [Calls] [int] NOT NULL,
  [AutomaticResponses] [int] NOT NULL,
  [Orders] [int] NOT NULL,
  [IssuesRaised] [smallint] NOT NULL,
  [AverageTimePerIssue] [smallint] NOT NULL,
  [ServiceGrade] [float] NOT NULL,
  [Date] [datetime] NULL,
  CONSTRAINT [PK_FactCallCenter_FactCallCenterID] PRIMARY KEY CLUSTERED ([FactCallCenterID]),
  CONSTRAINT [AK_FactCallCenter_DateKey_Shift] UNIQUE ([DateKey], [Shift])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[FactCallCenter]
  ADD CONSTRAINT [FK_FactCallCenter_DimDate] FOREIGN KEY ([DateKey]) REFERENCES [dbo].[DimDate] ([DateKey])
GO