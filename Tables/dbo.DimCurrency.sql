﻿CREATE TABLE [dbo].[DimCurrency] (
  [CurrencyKey] [int] IDENTITY,
  [CurrencyAlternateKey] [nchar](3) NOT NULL,
  [CurrencyName] [nvarchar](50) NOT NULL,
  CONSTRAINT [PK_DimCurrency_CurrencyKey] PRIMARY KEY CLUSTERED ([CurrencyKey])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [AK_DimCurrency_CurrencyAlternateKey]
  ON [dbo].[DimCurrency] ([CurrencyAlternateKey])
  ON [PRIMARY]
GO