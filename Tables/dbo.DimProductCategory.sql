﻿CREATE TABLE [dbo].[DimProductCategory] (
  [ProductCategoryKey] [int] IDENTITY,
  [ProductCategoryAlternateKey] [int] NULL,
  [EnglishProductCategoryName] [nvarchar](50) NOT NULL,
  [SpanishProductCategoryName] [nvarchar](50) NOT NULL,
  [FrenchProductCategoryName] [nvarchar](50) NOT NULL,
  CONSTRAINT [PK_DimProductCategory_ProductCategoryKey] PRIMARY KEY CLUSTERED ([ProductCategoryKey]),
  CONSTRAINT [AK_DimProductCategory_ProductCategoryAlternateKey] UNIQUE ([ProductCategoryAlternateKey])
)
ON [PRIMARY]
GO