﻿CREATE TABLE [dbo].[sysdiagrams] (
  [name] [sysname] NOT NULL,
  [principal_id] [int] NOT NULL,
  [diagram_id] [int] IDENTITY,
  [version] [int] NULL,
  [definition] [varbinary](max) NULL,
  CONSTRAINT [PK__sysdiagr__C2B05B616B24EA82] PRIMARY KEY CLUSTERED ([diagram_id])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

CREATE UNIQUE INDEX [UK_principal_name]
  ON [dbo].[sysdiagrams] ([principal_id], [name])
  ON [PRIMARY]
GO