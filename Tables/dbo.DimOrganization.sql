﻿CREATE TABLE [dbo].[DimOrganization] (
  [OrganizationKey] [int] IDENTITY,
  [ParentOrganizationKey] [int] NULL,
  [PercentageOfOwnership] [nvarchar](16) NULL,
  [OrganizationName] [nvarchar](50) NULL,
  [CurrencyKey] [int] NULL,
  CONSTRAINT [PK_DimOrganization] PRIMARY KEY CLUSTERED ([OrganizationKey])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[DimOrganization]
  ADD CONSTRAINT [FK_DimOrganization_DimCurrency] FOREIGN KEY ([CurrencyKey]) REFERENCES [dbo].[DimCurrency] ([CurrencyKey])
GO

ALTER TABLE [dbo].[DimOrganization]
  ADD CONSTRAINT [FK_DimOrganization_DimOrganization] FOREIGN KEY ([ParentOrganizationKey]) REFERENCES [dbo].[DimOrganization] ([OrganizationKey])
GO