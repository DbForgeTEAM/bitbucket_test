﻿CREATE TABLE [dbo].[DimSalesTerritory] (
  [SalesTerritoryKey] [int] IDENTITY,
  [SalesTerritoryAlternateKey] [int] NULL,
  [SalesTerritoryRegion] [nvarchar](50) NOT NULL,
  [SalesTerritoryCountry] [nvarchar](50) NOT NULL,
  [SalesTerritoryGroup] [nvarchar](50) NULL,
  [SalesTerritoryImage] [varbinary](max) NULL,
  CONSTRAINT [PK_DimSalesTerritory_SalesTerritoryKey] PRIMARY KEY CLUSTERED ([SalesTerritoryKey]),
  CONSTRAINT [AK_DimSalesTerritory_SalesTerritoryAlternateKey] UNIQUE ([SalesTerritoryAlternateKey])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO