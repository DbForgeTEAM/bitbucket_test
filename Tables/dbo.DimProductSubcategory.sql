﻿CREATE TABLE [dbo].[DimProductSubcategory] (
  [ProductSubcategoryKey] [int] IDENTITY,
  [ProductSubcategoryAlternateKey] [int] NULL,
  [EnglishProductSubcategoryName] [nvarchar](50) NOT NULL,
  [SpanishProductSubcategoryName] [nvarchar](50) NOT NULL,
  [FrenchProductSubcategoryName] [nvarchar](50) NOT NULL,
  [ProductCategoryKey] [int] NULL,
  CONSTRAINT [PK_DimProductSubcategory_ProductSubcategoryKey] PRIMARY KEY CLUSTERED ([ProductSubcategoryKey]),
  CONSTRAINT [AK_DimProductSubcategory_ProductSubcategoryAlternateKey] UNIQUE ([ProductSubcategoryAlternateKey])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[DimProductSubcategory]
  ADD CONSTRAINT [FK_DimProductSubcategory_DimProductCategory] FOREIGN KEY ([ProductCategoryKey]) REFERENCES [dbo].[DimProductCategory] ([ProductCategoryKey])
GO